<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
	<link rel="canonical" href="rtmp-module/">
	<meta name="robots" content="index, follow">
    <link rel="shortcut icon" type="image/png" href="favicon.png">
    
	<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css?6079">
	<link rel="stylesheet" type="text/css" href="style.css?8338">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    <script>
      $(document).ready(function() {
        // Initialize tooltips
        $('[data-toggle="tooltip"]').tooltip();

        setInterval(function() {
          $("#bloc-2").load(window.location.href + " #bloc-2", function() {
            // Re-initialize tooltips after content update
            $('[data-toggle="tooltip"]').tooltip();
          });
        }, 10000);
      });
    </script>

    <title>RTMP2SRT</title>


    
<!-- Analytics -->
 
<!-- Analytics END -->
    
</head>
<body>

<!-- Preloader -->
<div id="page-loading-blocs-notifaction" class="page-preloader"></div>
<!-- Preloader END -->


<!-- Main container -->
<div class="page-container">
    
<!-- bloc-0 -->
<div class="bloc l-bloc" id="bloc-0">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col">
				<nav class="navbar navbar-light row navbar-expand-md flex-column" role="navigation">
					<a class="navbar-brand mx-auto" href="index.php"><picture><source type="image/webp" srcset="img/favicon.webp"><img src="img/favicon.png" alt="logo" width="250" height="101"></picture></a>
					<button id="nav-toggle" type="button" class="ui-navbar-toggler navbar-toggler border-0 p-0" data-toggle="collapse" data-target=".navbar-34817" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"><svg height="32" viewBox="0 0 32 32" width="32"><path class="svg-menu-icon " d="m2 9h28m-28 7h28m-28 7h28"></path></svg></span>
					</button>
					<div class="collapse navbar-collapse navbar-34817">
							<ul class="site-navigation nav navbar-nav mx-auto justify-content-center">
								<li class="nav-item">
									<a href="index.php" class="nav-link a-btn ltc-4866">RTMP2SRT</a>
								</li>
							</ul>
						</div>
				</nav>
			</div>
		</div>
	</div>
</div>
<!-- bloc-0 END -->

<!-- bloc-1 -->
<div class="bloc l-bloc" id="bloc-1">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col text-left">
				<div class="divider-h">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-1 END -->

<!-- bloc-2 -->
<div class="bloc l-bloc " id="bloc-2">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col-md-2 text-left align-self-center text-lg-left col-lg-2 mr-lg-0 pr-lg-0">
				<div class="form-group text-lg-right">
					<div class="form-group">
<?php
$px = shell_exec('cat px');
$rks = shell_exec('cat rks');
if($px == 1){
        echo "                                  <a class=\"form-control\" rows\"1\" cols=\"50\">${rks} Kbps</a>";
}
?>
					</div>
				</div>
			</div>
			<div class="col-md-2 text-left mr-lg-0 pr-lg-0">
			<a href="index.php" class="btn btn-lg btn-block <?php $clr=shell_exec('cat 1935');echo $clr; ?>" data-placement="top" data-toggle="tooltip" title="URL rtmp://34.204.13.111:1935/live">RTMP</a>
			</div>
			<div class="col-md-2 text-left ml-lg-0 mr-lg-0 pl-lg-0 pr-lg-0">
				<div class="divider-h divider-background-color">
				</div>
			</div>
			<div class="col-md-2 text-left ml-lg-0 mr-lg-0 pl-lg-0 pr-lg-0">
				<div class="divider-h divider-0-background-color">
				</div>
			</div>
			<div class="col-md-2 text-left ml-lg-0 pl-lg-0">
				<a href="index.php" class="btn btn-lg btn-block <?php $clr=shell_exec('cat 4200');echo $clr; ?>" data-placement="top" data-toggle="tooltip" title="SRT Listener on port 4200">SRT</a>
			</div>
			<div class="col-md-2 text-left text-lg-left col-lg-2 align-self-center ml-lg-0 pl-lg-0">
				<div class="form-group">
					<div class="form-group">
<?php
$px = shell_exec('cat px');
$sks = shell_exec('cat sks');
if($px == 1){
	echo "					<a class=\"form-control\" rows=\"1\" cols=\"50\">${sks} Kbps</a>";
}
?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-2 END -->

<!-- bloc-4 -->
<div class="bloc l-bloc" id="bloc-4">
	<div class="container bloc-lg bloc-lg-lg">
		<div class="row">
			<div class="col text-md-left text-center">
				<div class="divider-h">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-4 END -->

<!-- bloc-5 -->
<div class="bloc l-bloc" id="bloc-5">
	<div class="container bloc-lg bloc-sm-lg">
		<div class="row">
			<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-md-left text-center">
<?php
$px = shell_exec('cat px');
if($px == 0){
	echo '			<a href="start.php" class="btn btn-lg btn-block btn-rd btn-c-5999">START</a>';
}

if($px == 1){
	echo "                  <a href='stop.php' class='btn btn-lg btn-block btn-rd btn-c-3482'>STOP</a>";
}
?>
			</div>
		</div>
	</div>
</div>
<!-- bloc-5 END -->

<!-- bloc-6 -->
<div class="bloc l-bloc none" id="bloc-6">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col text-md-left text-center">
				<div class="divider-h">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-6 END -->

<!-- ScrollToTop Button -->
<button aria-label="Scroll to top button" class="bloc-button btn btn-d scrollToTop" onclick="scrollToTarget('1',this)"><svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 32 32"><path class="scroll-to-top-btn-icon" d="M30,22.656l-14-13-14,13"/></svg></button>
<!-- ScrollToTop Button END-->


</div>
<!-- Main container END -->
    


<!-- Additional JS -->
<script src="./js/jquery.min.js?2432"></script>
<script src="./js/bootstrap.bundle.min.js?7465"></script>
<script src="./js/blocs.min.js?78"></script>
<script src="./js/lazysizes.min.js" defer></script><!-- Additional JS END -->


</body>
</html>
