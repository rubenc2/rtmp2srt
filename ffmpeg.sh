#!/bin/bash

while true; do
  if ! pgrep -x "ffmpeg" > /dev/null; then
    ffmpeg -re -listen 1 -i rtmp://0.0.0.0:1935/live -c:v copy -c:a copy -f mpegts udp://127.0.0.1:1234?pkt_size=1316 > /dev/null 2>&1 < /dev/null &
  fi
  if ! pgrep -x "srt-live-" > /dev/null; then
    srt-live-transmit -statspf:csv -statsout:/var/www/html/srt-rcv.csv -stats-report-frequency:500 "udp://:1234" "srt://:4200?mode=listener&latency=2000" > /dev/null 2>&1 &
  fi
  sleep 5
done

