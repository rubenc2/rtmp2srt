#!/bin/bash

if [ $# -lt 2 ]
  then
    echo "Please provide two port numbers to monitor."
    exit 1
fi

port1=$1
port2=$2
output_file1="/tmp/tcpdump_${port1}.cap"
output_file2="/tmp/tcpdump_${port2}.cap"

# Function to check if a port is in the ESTABLISHED state
is_port_established() {
    local port=$1
    netstat -tunapl 2>/dev/null | grep -w $port | grep -q ESTABLISHED
    return $?
}

# Function to start tcpdump for a given port and output file
start_tcpdump() {
    local port=$1
    local output_file=$2
    local duration=$3
    tcpdump -i any -nn -s 0 port $port -w $output_file -G $duration -W 1 > /dev/null 2>&1 &
}

# Function to calculate the bit rate
calculate_bit_rate() {
    local output_file=$1
    local duration=$2
    packet_size_bytes=$(du -b $output_file | awk '{print $1}')
    bit_rate_kbps=$(echo "scale=2; $packet_size_bytes * 8 / $duration / 1024" | bc)
    echo $bit_rate_kbps
}

# Monitoring loop
while true; do
    if is_port_established $port1; then
        start_tcpdump $port1 $output_file1 10
        sleep 10
        bit_rate_kbps1=$(calculate_bit_rate $output_file1 10)
	if [[ $bit_rate_kbps1 == "0" ]]; then
		kill -9 $(ps -ef | grep -e "ffmpeg -re -listen 1" | grep -v grep | awk '{print $2}')
	fi
	echo "btn-c-3482" > /var/www/html/1935
        echo "$bit_rate_kbps1" > /var/www/html/rks
    else
        echo "btn-c-3376" > /var/www/html/1935
	echo "0" > /var/www/html/rks
    fi
    start_tcpdump $port2 $output_file2 10
    sleep 10
    echo "btn-c-3482" > /var/www/html/4200
    bit_rate_kbps2=$(calculate_bit_rate $output_file2 10)
    echo "$bit_rate_kbps2" > /var/www/html/sks
    sleep 10
done

